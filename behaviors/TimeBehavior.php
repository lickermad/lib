<?php

namespace app\behaviors;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;

class TimeBehavior extends TimestampBehavior {

	/**
	 * @param $event
	 * @return string
	 * @throws InvalidConfigException
	 */
	public function getValue($event) {

		if ($this->value === null) {
			return Yii::$app->formatter->asTimestamp(Yii::$app->formatter->asDate(time()));
		}

		return parent::getValue($event);
	}
}
