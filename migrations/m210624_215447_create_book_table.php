<?php

use yii\db\Migration;

/**
 * Handles the creation of table `book`.
 */
class m210624_215447_create_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('{{%book}}', [
		    'id' => $this->primaryKey(),
		    'name' => $this->string()->notNull(),
		    'author' => $this->string()->notNull(),
		    'article' => $this->string(32),
		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
		    'status' => $this->smallInteger()->notNull()->defaultValue(0),
	    ]);

	    $this->createIndex('idx-book-name', '{{%book}}', 'name', true);
	    $this->createIndex('idx-book-author', '{{%book}}', 'author', true);
	    $this->createIndex('idx-book-article', '{{%book}}', 'article', true);

	    $this->batchInsert('{{%book}}',
		    [
			    'name',
			    'author',
			    'article',
			    'created_at',
			    'updated_at',
			    'status',
		    ],[
			    [
			    	'Web Application Development with Yii 2 and PHP',
				    'Mark Safronov',
				    '1783981881',
				    1624752000,
				    1624752000,
				    1,
			    ],
			    [
				    'Python for Data Analysis: Data Wrangling with Pandas, NumPy, and IPython 2nd Edition',
				    'Wes McKinney',
				    '1491957662',
				    1624752000,
				    1624752000,
				    2,
			    ],
			    [
				    'Hands-On Data Analysis with Pandas: Efficiently perform data collection, wrangling, analysis, and visualization using Python',
				    'Stefanie Molin',
				    '1789615321',
				    1624752000,
				    1624752000,
				    3,
			    ],
			    [
				    'Python Data Science Handbook: Essential Tools for Working with Data 1st Edition',
				    'Jake VanderPlas',
				    '1491912057',
				    1624752000,
				    1624752000,
				    2,
			    ],
	        ]
	    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('{{%book}}');
    }
}
