<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lease`.
 */
class m210627_085106_create_lease_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%lease}}', [
			'id' => $this->primaryKey(),

			'issue_at' => $this->integer()->notNull(),
			'return_at' => $this->integer()->notNull(),

			'book_id' => $this->integer()->notNull(),
			'user_id' => $this->integer()->notNull(),
			'condition_id' => $this->integer()->notNull(),
		]);

		$this->addForeignKey(
			'fk__lease_book__book_id',
			'{{%lease}}',
			'book_id',
			'{{%book}}',
			'id'
		);
		$this->addForeignKey(
			'fk__lease_user__user_id',
			'{{%lease}}',
			'user_id',
			'{{%user}}',
			'id'
		);
		$this->addForeignKey(
			'fk__lease_condition__condition_id',
			'{{%lease}}',
			'condition_id',
			'{{%condition}}',
			'id'
		);


	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropForeignKey('fk__lease_book__book_id', '{{%lease}}');
		$this->dropForeignKey('fk__lease_user__user_id', '{{%lease}}');
		$this->dropForeignKey('fk__lease_condition__condition_id', '{{%lease}}');

		$this->dropTable('{{%lease}}');
	}
}
