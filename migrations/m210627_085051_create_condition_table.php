<?php

use yii\db\Migration;

/**
 * Handles the creation of table `condition`.
 */
class m210627_085051_create_condition_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%condition}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
		]);

		$this->createIndex('idx-condition-name', '{{%condition}}', 'name', true);

		$this->batchInsert('{{%condition}}', ['name'],[
			['Новая'],
			['Порвана обложка'],
			['Не хватает страниц'],
			['Рисунки ручкой'],
			['Плохое'],
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%condition}}');
	}
}
