<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m210624_101825_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('{{%user}}', [
		    'id' => $this->primaryKey(),
		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
		    'username' => $this->string(64)->notNull(),
		    'fullname' => $this->string(128)->notNull(),
		    'passport' => $this->string(16)->notNull(),
		    'role' => $this->smallInteger()->notNull()->defaultValue(0),
		    'auth_key' => $this->string(32),
		    'email_confirm_token' => $this->string(),
		    'password_hash' => $this->string()->notNull(),
		    'password_reset_token' => $this->string(),
		    'email' => $this->string()->notNull(),
		    'status' => $this->smallInteger()->notNull()->defaultValue(0),
	    ]);

	    $this->createIndex('idx-user-username', '{{%user}}', 'username', true);
	    $this->createIndex('idx-user-email', '{{%user}}', 'email', true);

	    $this->batchInsert('{{%user}}',
		    [
		    	'created_at',
			    'updated_at',
			    'username',
			    'fullname',
			    'passport',
			    'role',
			    'email',
			    'status',
			    'password_hash',
			    'auth_key'
		    ],[
		    [
			    time(),
			    time(),
			    'admin',
			    'Иванов Серж',
			    'MR939772',
			    1,
			    'admin@nodomain.us',
			    1,
			    Yii::$app->security->generatePasswordHash('access'),
			    Yii::$app->security->generateRandomString()
		    ],
	    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('{{%user}}');
    }
}
