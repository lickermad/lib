<?php

namespace app\controllers;

use app\models\Lease;
use app\models\LeaseForm;
use app\models\LeaseSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;
use app\models\UserForm;
use app\models\EmailConfirm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Book;
use app\models\BookSearch;
use app\models\User;
use app\models\UserSearch;

class SiteController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout', 'clients', 'users'],
				'rules' => [
					[
						'actions' => ['logout', 'index', 'clients', 'users'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
				'denyCallback' => function ($rule, $action) {
					$this->redirect('/login');
				}
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['GET'],
					'index' => ['POST', 'GET'],
					'users' => ['POST', 'GET'],
					'leases' => ['POST', 'GET'],
				],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * @brief Login action
	 * @return Response|string
	 */
	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->redirect('/');
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->redirect('/'); // $this->goBack();
		}

		$model->password = '';
		return $this->render('login', [
			'model' => $model,
		]);
	}

	/**
	 * @brief Logout action
	 * @return Response
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->redirect('/');
	}

	/**
	 * @return string|Response
	 * @throws \yii\base\Exception
	 */
	public function actionSignup()
	{
		$model = new UserForm();
		if ($model->load(Yii::$app->request->post())) {
			if ($user = $model->signup()) {
				Yii::$app->getSession()->setFlash('success', 'Подтвердите ваш электронный адрес.');
				return $this->goHome();
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	/**
	 * @param $token
	 * @return Response
	 */
	public function actionEmailConfirm($token)
	{
		try {
			$model = new EmailConfirm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->confirmEmail()) {
			Yii::$app->getSession()->setFlash('success', 'Спасибо! Ваш Email успешно подтверждён.');
		} else {
			Yii::$app->getSession()->setFlash('error', 'Ошибка подтверждения Email.');
		}

		return $this->goHome();
	}

	/**
	 * @return string|Response
	 */
	public function actionPasswordResetRequest()
	{
		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->getSession()->setFlash('success', 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.');

				return $this->goHome();
			} else {
				Yii::$app->getSession()->setFlash('error', 'Извините. У нас возникли проблемы с отправкой.');
			}
		}

		return $this->render('passwordResetRequest', [
			'model' => $model,
		]);
	}

	/**
	 * @param $token
	 * @return string|Response
	 * @throws \yii\base\Exception
	 */
	public function actionPasswordReset($token)
	{
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->getSession()->setFlash('success', 'Спасибо! Пароль успешно изменён.');

			return $this->goHome();
		}

		return $this->render('passwordReset', [
			'model' => $model,
		]);
	}

	/**
	 * @brief Стартовая страница
	 * @return string
	 * @throws \yii\base\Exception
	 */
	public function actionIndex()
	{
		$action = Yii::$app->request->get('action', 'create');
		$id = Yii::$app->request->get('id', 0);

		switch ($action) {
			case 'update':
				$modelBook = Book::findOne($id);
				break;

			case 'delete':
				$leases = Lease::findAll(['book_id' => $id]);
				if ($leases) {
					Yii::$app->session->setFlash('error', 'Книга имеет историю и не может быть удалена.');
				} else {
					Book::deleteAll(['id' => $id]);
				}
				$modelBook = new Book();
				break;

			case 'create':
				$modelBook = new Book();
				break;
		}

		if (!Yii::$app->user->isGuest &&
			$modelBook->load(Yii::$app->request->post()) &&
			$modelBook->save()) {

			Yii::$app->session->setFlash('info', 'Книга успешно ' .
				($action == 'create' ? 'добавлена' : 'сохранена') . '.');

			return $this->redirect(['/']);
		}

		$searchModel = new BookSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->get());

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'modelBook' => $modelBook,
			'action' => $action
		]);
	}

	/**
	 * @brief Страница пользователей
	 * @return string|Response
	 * @throws \yii\base\Exception
	 */
	public function actionUsers()
	{
		$action = Yii::$app->request->get('action', 'create');
		$id = Yii::$app->request->get('id', 0);

		switch ($action) {
			case 'update':
				$user = User::findOne($id);
				$modelUser = new UserForm();
				$modelUser->load($user->toArray(), '');
				break;

			case 'delete':
				$leases = Lease::findAll(['user_id' => $id]);
				if ($leases) {
					Yii::$app->session->setFlash('error', 'Пользователь имеет историю и не может быть удален.');
				} else {
					User::deleteAll(['id' => $id]);
				}
				$modelUser = new UserForm();
				break;

			case 'create':
				$modelUser = new UserForm();
				break;
		}

		if (!Yii::$app->user->isGuest &&
			$modelUser->load(Yii::$app->request->post()) &&
			$modelUser->update($id)) {

			Yii::$app->session->setFlash('info', 'Пользователь успешно ' .
				($action == 'create' ? 'добавлен' : 'сохранен') . '.');

			return $this->redirect(['/users']);
		}

		$searchModel = new UserSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->get());

		return $this->render('users', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'modelUser' => $modelUser,
			'action' => $action
		]);
	}

	/**
	 * @brief Выдача/сдача книг
	 * @return string|Response
	 * @throws \yii\base\Exception
	 */
	public function actionLeases()
	{
		$action = Yii::$app->request->get('action', 'create');
		$id = Yii::$app->request->get('id', 0);

		switch ($action) {
			case 'update':
				$modelLease = Lease::findOne($id);
				break;

			case 'delete':
				Lease::deleteAll(['id' => $id]);
				$modelLease = new Lease();
				break;

			case 'create':
				$modelLease = new Lease();
				break;
		}

		if (!Yii::$app->user->isGuest &&
			$modelLease->load(Yii::$app->request->post()) &&
			$modelLease->save()) {

			Yii::$app->session->setFlash('info', 'Операция успешно ' .
				($action == 'create' ? 'добавлена' : 'сохранена') . '.');

			return $this->redirect(['/leases']);
		}

		$searchModel = new LeaseSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->get());

		return $this->render('leases', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'modelLease' => $modelLease,
			'action' => $action
		]);
	}

}
