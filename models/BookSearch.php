<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Book;

class BookSearch extends Book
{
	public function rules()
	{
		// только поля определенные в rules() будут доступны для поиска
		return [
			[['id', 'status', 'created_at'], 'integer'],
			[['name', 'author', 'article'], 'string', 'max' => 255],
			['created_at_formatted', 'safe']
		];
	}

	public function scenarios()
	{
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = Book::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
			],
		]);

		// загружаем данные формы поиска и производим валидацию
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		// изменяем запрос добавляя в его фильтрацию
		$query->andFilterWhere(['id' => $this->id]);
		$query->andFilterWhere(['like', 'name', $this->name]);
		$query->andFilterWhere(['like', 'author', $this->author]);
		$query->andFilterWhere(['like', 'article', $this->article]);

		if ($this->created_at) {
			$query->andFilterWhere(['created_at' => Yii::$app->formatter->asTimestamp($this->created_at)]);
		}

		$query->andFilterWhere(['status' => $this->status]);

		return $dataProvider;
	}
}
