<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id ID
 * @property int $created_at Created
 * @property int $updated_at Updated
 * @property string $username Username
 * @property string $fullname Fullname
 * @property int $role Role
 * @property string $auth_key Auth key
 * @property string $email_confirm_token Email confirm
 * @property string $password_hash Password hash
 * @property string $password_reset_token Password reset
 * @property string $email Email
 * @property int $status Status
 */
class User extends ActiveRecord implements IdentityInterface
{
	const STATUS_BLOCKED = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_WAIT = 2;

	const ROLE_USER = 0;
	const ROLE_ADMIN = 1;
	const ROLE_CLIENT = 2;
	const ROLE_STAFF = 3;
	const ROLE_CLEANER = 4;
	const ROLE_WATCHMAN = 5;

	public $leases;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        ['username', 'required'],
	        ['username', 'match', 'pattern' => '#^[\w_-]+$#is'],
	        ['username', 'unique', 'targetClass' => self::className(), 'message' => 'This username has already been taken.'],
	        ['username', 'string', 'min' => 2, 'max' => 255],

	        ['fullname', 'string', 'min' => 10, 'max' => 128],

	        ['email', 'required'],
	        ['email', 'email'],
	        ['email', 'unique', 'targetClass' => self::className(), 'message' => 'This email address has already been taken.'],
	        ['email', 'string', 'max' => 255],

	        ['role', 'integer'],
	        ['role', 'default', 'value' => self::ROLE_USER],
	        ['role', 'in', 'range' => array_keys(self::getRolesArray())],

	        ['status', 'integer'],
	        ['status', 'default', 'value' => self::STATUS_ACTIVE],
	        ['status', 'in', 'range' => array_keys(self::getStatusesArray())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
	        'id' => 'ID',
	        'created_at' => 'Создан',
	        'updated_at' => 'Изменён',
	        'username' => 'Логин',
	        'fullname' => 'ФИО',
	        'role' => 'Должность',
	        'passport' => 'Паспорт',
	        'email' => 'E-mail',
	        'status' => 'Статус',
	        'leases' => 'Взял книг'
        ];
    }

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * @param int|string $id
	 * @return User|IdentityInterface|null
	 */
	public static function findIdentity($id)
	{
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * @param mixed $token
	 * @param null $type
	 * @return void|IdentityInterface
	 * @throws NotSupportedException
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
	}

	/**
	 * @return int|mixed|string
	 */
	public function getId()
	{
		return $this->getPrimaryKey();
	}

	/**
	 * @return string
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * @param string $authKey
	 * @return bool
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 * @return static|null
	 */
	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * @param string $password
	 * @throws \yii\base\Exception
	 */
	public function setPassword($password)
	{
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey()
	{
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token)
	{
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}
		return static::findOne([
			'password_reset_token' => $token,
			'status' => self::STATUS_ACTIVE,
		]);
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 * @return boolean
	 */
	public static function isPasswordResetTokenValid($token)
	{
		if (empty($token)) {
			return false;
		}
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts = explode('_', $token);
		$timestamp = (int) end($parts);
		return $timestamp + $expire >= time();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken()
	{
		$this->password_reset_token = null;
	}

	/**
	 * @param string $email_confirm_token
	 * @return static|null
	 */
	public static function findByEmailConfirmToken($email_confirm_token)
	{
		return static::findOne(['email_confirm_token' => $email_confirm_token, 'status' => self::STATUS_WAIT]);
	}

	/**
	 * Generates email confirmation token
	 */
	public function generateEmailConfirmToken()
	{
		$this->email_confirm_token = Yii::$app->security->generateRandomString();
	}

	/**
	 * Removes email confirmation token
	 */
	public function removeEmailConfirmToken()
	{
		$this->email_confirm_token = null;
	}

	/**
	 * @param bool $insert
	 * @return bool
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if ($insert) {
				$this->generateAuthKey();
			}
			return true;
		}
		return false;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLeases()
	{
		return $this->hasMany(Lease::className(), ['user_id' => 'id']);
	}

	/**
	 * {@inheritdoc}
	 */
	public function afterFind()
	{
		$this->leases = Lease::find()
			->where(['user_id' => $this->id])
			->count();
		return parent::afterFind();
	}

	/**
	 * @return mixed
	 */
	public function getStatusName()
	{
		return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
	}

	/**
	 * @return array
	 */
	public static function getStatusesArray()
	{
		return [
			self::STATUS_BLOCKED => 'Заблокирован',
			self::STATUS_ACTIVE => 'Активен',
			self::STATUS_WAIT => 'Проверка',
		];
	}

	/**
	 * @return mixed
	 */
	public function getRoleName()
	{
		return ArrayHelper::getValue(self::getRolesArray(), $this->role);
	}

	/**
	 * @return array
	 */
	public static function getRolesArray()
	{
		return [
			self::ROLE_USER => 'Пользователь',
			self::ROLE_ADMIN => 'Администратор',
			self::ROLE_CLIENT => 'Клиент',
			self::ROLE_STAFF => 'Сотрудник',
			self::ROLE_CLEANER => 'Уборщик',
			self::ROLE_WATCHMAN => 'Сторож',
		];
	}
}
