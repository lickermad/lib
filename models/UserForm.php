<?php

namespace app\models;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Signup form
 */
class UserForm extends Model
{
	public $username;
	public $fullname;
	public $email;
	public $passport;
	public $role;
	public $password;
	public $verifyCode;
	public $status;

	public function rules()
	{
		return [
			['username', 'filter', 'filter' => 'trim'],
			['username', 'required'],
			['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
			['username', 'unique', 'targetClass' => User::className(), 'message' => 'This username has already been taken.'],
			['username', 'string', 'min' => 2, 'max' => 255],

			['fullname', 'filter', 'filter' => 'trim'],
			['fullname', 'string', 'min' => 10, 'max' => 128],

			['email', 'filter', 'filter' => 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'unique', 'targetClass' => User::className(), 'message' => 'This email address has already been taken.'],

			['passport', 'filter', 'filter' => 'trim'],
//			['passport', 'match', 'pattern' => '#^[\w_-]+$#i'],
			['passport', 'unique', 'targetClass' => User::className(), 'message' => 'This passport has already been taken.'],
			['passport', 'string', 'min' => 2, 'max' => 16],

			['password', 'required'],
			['password', 'string', 'min' => 6],

			['role', 'integer'],
			['role', 'default', 'value' => User::ROLE_USER],
			['role', 'in', 'range' => array_keys(User::getRolesArray())],

			['status', 'integer'],
			['status', 'default', 'value' => User::STATUS_ACTIVE],
			['status', 'in', 'range' => array_keys(User::getStatusesArray())],

			['verifyCode', 'captcha', 'captchaAction' => '/site/captcha', 'message' => 'Captcha verify fail.'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'created_at' => 'Создан',
			'updated_at' => 'Изменён',
			'username' => 'Логин',
			'fullname' => 'ФИО',
			'role' => 'Должность',
			'passport' => 'Паспорт',
			'password' => 'Пароль',
			'email' => 'E-mail',
			'status' => 'Статус',
		];
	}

	/**
	 * Signs user up.
	 *
	 * @return User|null the saved model or null if saving fails
	 * @throws \yii\base\Exception
	 */
	public function signup()
	{
		if ($this->validate()) {

			$user = new User();
			$user->username = $this->username;
			$user->fullname = $this->fullname;
			$user->email = $this->email;
			$user->passport = $this->passport;
			$user->role = $this->role;
			$user->status = $this->status;
			$user->setPassword($this->password);
			$user->status = User::STATUS_WAIT;
			$user->generateAuthKey();
			$user->generateEmailConfirmToken();

			if ($user->save()) {
				Yii::$app->mailer->compose('@app/mail/user/emailConfirm', ['user' => $user])
					->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
					->setTo($this->email)
					->setSubject('Email confirmation for ' . Yii::$app->name)
					->send();
				return $user;
			}
		}

		return null;
	}

	public function update($id = 0)
	{
		if ($this->validate('username, email, passport, role, status, password')) {

			$user = User::findOne($id);
			$user->username = $this->username;
			$user->fullname = $this->fullname;
			$user->email = $this->email;
			$user->passport = $this->passport;
			$user->role = $this->role;
			$user->status = $this->status;
			$user->setPassword($this->password);
			$user->generateAuthKey();
			$user->generateEmailConfirmToken();

			return $user->save(false);
		}

		return null;
	}
}
