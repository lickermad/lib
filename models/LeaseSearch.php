<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lease;

class LeaseSearch extends Lease
{
	public function rules()
	{
		// только поля определенные в rules() будут доступны для поиска
		return [
			['id', 'integer'],
			[['issue_at', 'return_at', 'book_id', 'user_id', 'condition_id', 'status'], 'safe'],
		];
	}

	public function scenarios()
	{
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = Lease::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
			],
		]);

		// загружаем данные формы поиска и производим валидацию
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		// изменяем запрос добавляя в его фильтрацию
		$query->andFilterWhere(['id' => $this->id]);
		$query->andFilterWhere(['book_id' => $this->book_id]);
		$query->andFilterWhere(['user_id' => $this->user_id]);
		$query->andFilterWhere(['condition_id' => $this->condition_id]);

		if ($this->issue_at) {
			$query->andFilterWhere(['issue_at' => Yii::$app->formatter->asTimestamp($this->issue_at)]);
		}

		if ($this->return_at) {
			$query->andFilterWhere(['return_at' => Yii::$app->formatter->asTimestamp($this->return_at)]);
		}
var_dump($params, $this->status);
		if ($this->status) {
			$books = Book::find()
				->select('id as book_id')
				->groupBy(['id', 'status'])
				->having(['status' => $this->status])
				->asArray()
				->all();
			if (empty($books)) {
				$books[] = ['book_id' => 0];
			}
var_dump($books);
			$query->andFilterWhere(['in', 'book_id', $books]);
		}

		return $dataProvider;
	}
}
