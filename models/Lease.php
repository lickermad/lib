<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\models\Book;
use app\behaviors\DateToTimeBehavior;

/**
 * This is the model class for table "lease".
 *
 * @property int $id
 * @property int $issue_at
 * @property int $return_at
 * @property int $book_id
 * @property int $user_id
 * @property int $condition_id
 * @property int $status
 *
 */
class Lease extends ActiveRecord
{
	public $issue_at_formatted;
	public $return_at_formatted;
	public $status;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'lease';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			['id', 'integer'],

			[['book_id', 'user_id', 'condition_id', 'status'], 'required'],
			[['book_id', 'user_id', 'condition_id', 'status'], 'integer'],
			[
				'book_id',
				'exist',
				'skipOnError' => true,
				'targetClass' => Book::className(),
				'targetAttribute' => ['book_id' => 'id']
			],
			[
				'user_id',
				'exist',
				'skipOnError' => true,
				'targetClass' => User::className(),
				'targetAttribute' => ['user_id' => 'id']
			],
			[
				'condition_id',
				'exist',
				'skipOnError' => true,
				'targetClass' => Condition::className(),
				'targetAttribute' => ['condition_id' => 'id']
			],

			['issue_at', 'integer'],
			['issue_at_formatted', 'date', 'format' => 'php:Y-m-d'],

			['return_at', 'integer'],
			['return_at_formatted', 'date', 'format' => 'php:Y-m-d'],

		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'book_id' => 'Книга',
			'user_id' => 'Сотрудник',
			'condition_id' => 'Состояние',
			'issue_at' => 'Выдана',
			'issue_at_formatted' => 'Выдана',
			'return_at' => 'Возврат',
			'return_at_formatted' => 'Возврат',
			'status' => 'Статус'
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'issue' => [
				'class' => DateToTimeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_VALIDATE => 'issue_at_formatted',
					ActiveRecord::EVENT_AFTER_FIND => 'issue_at_formatted',
				],
	  			'timeAttribute' => 'issue_at',
	    		'timeFormat' => 'Y-m-d'
			],
			'return' => [
				'class' => DateToTimeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_VALIDATE => 'return_at_formatted',
					ActiveRecord::EVENT_AFTER_FIND => 'return_at_formatted',
				],
				'timeAttribute' => 'return_at',
				'timeFormat' => 'Y-m-d'
			]

		];
	}

	/**
	 * @param bool $insert
	 * @return bool
	 */
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			$this->book->status = $this->status;
			$this->book->save();
			return true;
		}
		return false;
	}

	/**
	 * {@inheritdoc}
	 */
	public function afterFind()
	{
		$this->status = $this->book->status;
		return parent::afterFind();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBook()
	{
		return $this->hasOne(Book::className(), ['id' => 'book_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCondition()
	{
		return $this->hasOne(Condition::className(), ['id' => 'condition_id']);
	}

}
