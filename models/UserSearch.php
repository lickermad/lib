<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

class UserSearch extends User
{
	public function rules()
	{
		// только поля определенные в rules() будут доступны для поиска
		return [
			[['id', 'role', 'status', 'leases'], 'integer'],
			[['username', 'email', 'passport', 'fullname'], 'string', 'max' => 255],
		];
	}

	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = User::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
			],
		]);

		// загружаем данные формы поиска и производим валидацию
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		// изменяем запрос добавляя в его фильтрацию
		$query->andFilterWhere(['id' => $this->id]);
		$query->andFilterWhere(['like', 'username', $this->username]);
		$query->andFilterWhere(['like', 'fullname', $this->fullname]);
		$query->andFilterWhere(['like', 'email', $this->email]);
		$query->andFilterWhere(['like', 'passport', $this->passport]);
		$query->andFilterWhere(['role' => $this->role]);
		$query->andFilterWhere(['status' => $this->status]);

		if ($this->leases) {
			$users = Lease::find()
				->select('user_id')
				->groupBy(['user_id', 'book_id'])
				->having(['count(book_id)' => $this->leases])
				->asArray()
				->all();
			if (empty($users)) {
				$users[] = ['user_id' => 0];
			}
			$query->andFilterWhere(['in', 'id', $users]);
		}

		return $dataProvider;
	}
}
