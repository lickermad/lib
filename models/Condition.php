<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "condition".
 *
 * @property int $id
 * @property string $name
 *
 */
class Condition extends ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'condition';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			['id', 'integer'],
			['name', 'string', 'max' => 255],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLeases()
	{
		return $this->hasMany(Lease::className(), ['condition_id' => 'id']);
	}

}
