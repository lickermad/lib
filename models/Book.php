<?php

namespace app\models;

use app\behaviors\DateToTimeBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\behaviors\TimeBehavior;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $name
 * @property string $author
 * @property string $article
 * @property int $created_at
 * @property int $updated_at
 * @property int $status
 *
 */
class Book extends ActiveRecord
{
	const STATUS_MISS = 1;
	const STATUS_IN_STOCK = 2;
	const STATUS_IN_USE = 3;

	public $created_at_formatted;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'book';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id', 'created_at', 'updated_at', 'status'], 'integer'],
			['name', 'string', 'max' => 255],
			['author', 'string', 'max' => 255],
			['article', 'string', 'max' => 32],

			['created_at_formatted', 'date', 'format' => 'php:Y-m-d'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
			'author' => 'Автор',
			'article' => 'Артикул',
			'created_at' => 'Добавлена',
			'updated_at' => 'Изменена',
			'status' => 'Статус'
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			TimeBehavior::className(),
			'created' => [
				'class' => DateToTimeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_VALIDATE => 'created_at_formatted',
					ActiveRecord::EVENT_AFTER_FIND => 'created_at_formatted',
				],
				'timeAttribute' => 'created_at',
				'timeFormat' => 'Y-m-d'
			],
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLeases()
	{
		return $this->hasMany(Lease::className(), ['book_id' => 'id']);
	}


	public static function getStock($all = false)
	{
		if ($all)
			return Book::find()->asArray()->all();
		else
			return Book::find()->where(['status' => Book::STATUS_IN_STOCK])->asArray()->all();
	}

	/**
	 * @return mixed
	 */
	public function getStatusName()
	{
		return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
	}

	/**
	 * @return array
	 */
	public static function getStatusesArray()
	{
		return [
			self::STATUS_MISS => 'Утеряна',
			self::STATUS_IN_USE => 'Выдана',
			self::STATUS_IN_STOCK => 'На складе',
		];
	}

}
