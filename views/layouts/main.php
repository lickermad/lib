<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

if (Yii::$app->user->isGuest) {
	$items = [
		['label' => 'Books', 'url' => ['/']],
		['label' => 'Sign Up', 'url' => ['/signup']],
		['label' => 'Login', 'url' => ['/login']]
	];
} else {
	$items = [
		['label' => 'Books', 'url' => ['/']],
		['label' => 'Users', 'url' => ['/users']],
		['label' => 'Leases', 'url' => ['/leases']],
		['label' => 'Exit', 'url' => ['/logout']],
	];
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

	<?php NavBar::begin([
		'brandLabel' => '',
		'brandUrl' => \yii\helpers\Url::to('/'),
		'options' => [
			'class' => 'navbar-inverse navbar-fixed-top',
		],
	]);
		echo Nav::widget(['options' => ['class' => 'navbar-nav navbar-right'], 'items' => $items]);
	NavBar::end(); ?>

    <div class="container page-wrap">
	    <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
