<?php

use app\models\Book;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $modelBook \app\models\Book */
/* @var $action string */

$this->title = 'Library';
$this->params['breadcrumbs'][] = $this->title;

Icon::map($this);
$winner = false;

?>
<div class="site-books">

	<?php Pjax::begin(); ?>

		<?php if (!Yii::$app->user->isGuest) : ?>
			<?php $form = ActiveForm::begin(); ?>
				<div class="row">
					<div class="col-md-4">
						<?= $form->field($modelBook, 'name')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-md-3">
						<?= $form->field($modelBook, 'author')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-md-2">
						<?= $form->field($modelBook, 'article')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-md-3">
						<div class="row">
							<?= Html::submitButton(($action == 'create' ? 'Добавить' : 'Сохранить'), [
								'class' => 'btn btn-success col-md-offset-2 col-md-9',
								'style' => 'margin-top: 24px;'
							]) ?>
						</div>
					</div>
				</div>

				<?= $form->field($modelBook, 'status')->hiddenInput(['value' => 2])->label(false) ?>

			<?php ActiveForm::end(); ?>
		<?php endif; ?>

		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'layout'=>"{pager}\n{summary}\n{items}\n{pager}",
			'pager' => [
				'maxButtonCount' => 10,
				'nextPageLabel' => false,
				'prevPageLabel' => false,
			],
			'tableOptions' => [
				'class' => 'table table-striped table-bordered'
			],
			'columns' => [
				[
					'attribute' => 'id',
					'headerOptions' => ['width' => '70'],
				],
				'name',
				[
					'attribute' => 'author',
					'headerOptions' => ['width' => '180'],
				],
				[
					'attribute' => 'article',
					'headerOptions' => ['width' => '120'],
				],
				[
					'attribute' => 'created_at',
					'value' => function ($row) {
						return Yii::$app->formatter->asDate($row->created_at);
					},
					'filter' => DatePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at_formatted',
						'language' => 'ru',
						'dateFormat' => 'yyyy-MM-dd',
						'options' => ['class' => 'form-control']
					]),
					'format' => 'html',
					'headerOptions' => ['width' => '120'],
				],
				[
					'attribute' => 'status',
					'value' => function ($row) {
						return ArrayHelper::getValue(Book::getStatusesArray(), $row->status);
					},
					'filter' => ArrayHelper::toArray(Book::getStatusesArray()),
					'headerOptions' => ['width' => '110'],
				],
//				[
//					'label' => 'Читатель',
//					'value' => function ($row) {
//						return $row->getHolder();
//					},
//					'headerOptions' => ['width' => '160'],
//				],
				[
					'class' => 'yii\grid\ActionColumn',
					'headerOptions' => ['width' => '50'],
					'template' => '{update} {delete}',
					'urlCreator' => function ($action, $model, $key, $index) {
						$url = '/?action=' . $action . '&id=' . $model->id;
						return $url;
					},
					'visible' => !Yii::$app->user->isGuest,
				],
			],
		]); ?>

	<?php Pjax::end(); ?>

</div>

<?php

$js = <<<JS
JS;

$css = <<<CSS
CSS;

$this->registerJs($js);
$this->registerCss($css);
