<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\web\View;
use app\models\User;
use app\models\UserSearch;

use yii\widgets\Pjax;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel \app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $modelUser app\models\User */
/* @var $action string */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;

Icon::map($this);
$winner = false;

?>
	<div class="site-users">

		<?php Pjax::begin(); ?>

			<?php $form = ActiveForm::begin(); ?>

				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-4">
								<?= $form->field($modelUser, 'fullname')->textInput(['maxlength' => true]) ?>
							</div>
							<div class="col-md-2">
								<?= $form->field($modelUser, 'passport')->textInput(['maxlength' => true]) ?>
							</div>
							<div class="col-md-3">
								<?= $form->field($modelUser, 'role')->dropDownList(
									ArrayHelper::toArray(User::getRolesArray()), ['prompt' => 'Укажите должность']) ?>
							</div>
							<div class="col-md-3">
								<?= $form->field($modelUser, 'status')->dropDownList(
									ArrayHelper::toArray(User::getStatusesArray()), ['prompt' => 'Укажите статус']) ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<?= $form->field($modelUser, 'username')->textInput(['maxlength' => true]) ?>
							</div>
							<div class="col-md-4">
								<?= $form->field($modelUser, 'email')->textInput(['maxlength' => true]) ?>
							</div>
							<div class="col-md-4">
								<?= $form->field($modelUser, 'password')->passwordInput() ?>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="row text-right">
						<?= Html::submitButton($action == 'create' ? 'Создать' : 'Сохранить',
							['class' => 'btn btn-success col-md-offset-2 col-md-9', 'style' => 'margin-top: 25px;']) ?>
						</div>
						<div class="row">
							<?= Html::resetButton('Очистить',
								[
									'class' => 'btn btn-default col-md-offset-2 col-md-9', 'style' => 'margin-top: 40px;',
									'onclick' => "location.href='/users'"
								]) ?>
						</div>
					</div>
				</div>

			<?php ActiveForm::end(); ?>


			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'layout'=>"{pager}\n{summary}\n{items}\n{pager}",
				'pager' => [
					'maxButtonCount' => 10,
					'nextPageLabel' => false,
					'prevPageLabel' => false,
				],
				'tableOptions' => [
					'class' => 'table table-striped table-bordered'
				],
				'columns' => [
					[
						'attribute' => 'id',
						'headerOptions' => ['width' => '70'],
					],
					[
						'attribute' => 'username',
						'headerOptions' => ['width' => '150'],
					],
					[
						'attribute' => 'fullname',
					],
					[
						'attribute' => 'email',
						'headerOptions' => ['width' => '200'],
					],
					[
						'attribute' => 'passport',
						'value' => function ($row) {
							return isset($row->passport) ? $row->passport : '';
						},
						'headerOptions' => ['width' => '120'],
					],
					[
						'attribute' => 'role',
						'value' => function ($row) {
							return ArrayHelper::getValue(User::getRolesArray(), $row->role);
						},
						'filter' => ArrayHelper::toArray(User::getRolesArray()),
						'headerOptions' => ['width' => '150'],
					],
					[
						'attribute' => 'status',
						'value' => function ($row) {
							return ArrayHelper::getValue(User::getStatusesArray(), $row->status);
						},
						'filter' => ArrayHelper::toArray(User::getStatusesArray()),
						'headerOptions' => ['width' => '150'],
					],
					[
						'attribute' => 'leases',
						'value' => function ($row) {
							return $row->leases;
						},
						'headerOptions' => ['width' => '120'],
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'headerOptions' => ['width' => '50'],
						'template' => '{update} {delete}',
						'urlCreator' => function ($action, $model, $key, $index) {
							$url = '/users?action=' . $action . '&id=' . $model->id;
							return $url;
						},
					],
				],
			]); ?>

		<?php Pjax::end(); ?>

	</div>

<?php

$js = <<<JS
JS;

$css = <<<CSS
CSS;

$this->registerJs($js);
$this->registerCss($css);
