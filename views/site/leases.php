<?php

use app\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\web\View;
use app\models\Lease;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use app\models\Book;
use app\models\Condition;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\LeaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $modelLease app\models\Lease */
/* @var $action string */

$this->title = 'Leases';
$this->params['breadcrumbs'][] = $this->title;

Icon::map($this);
$winner = false;

?>
	<div class="site-leases">

		<?php Pjax::begin(); ?>

			<?php $form = ActiveForm::begin(['id' => 'lease-form']); ?>

				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-8">
								<?= $form->field($modelLease, 'book_id')->dropDownList(
									ArrayHelper::map(Book::getStock($action == 'update'),'id','name'),
									 ['prompt' => 'Выберите книгу', 'disabled' => $action == 'update']
								 ) ?>
							</div>
							<div class="col-md-4">
								<?= $form->field($modelLease, 'user_id')->dropDownList(
									ArrayHelper::map(User::find()->all(),'id','fullname'),
									['prompt' => 'Выберите сотрудника', 'disabled' => $action == 'update']
								) ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<?= $form->field($modelLease, 'issue_at_formatted')->widget(DatePicker::classname(), [
									'language' => 'ru',
									'dateFormat' => 'yyyy-MM-dd',
									'clientOptions' => [
									],
									'options' => [
										'class' => 'form-control',
										'placeholder' => Yii::$app->formatter->asDate(time(), 'php:Y-m-d'),
										'disabled' => $action == 'update'
									]
								]) ?>
							</div>
							<div class="col-md-3">
								<?= $form->field($modelLease, 'return_at_formatted')->widget(DatePicker::classname(), [
									'language' => 'ru',
									'dateFormat' => 'yyyy-MM-dd',
									'clientOptions' => [
									],
									'options' => [
										'class' => 'form-control',
										'placeholder' => Yii::$app->formatter->asDate(time(), 'php:Y-m-d'),
									]
								]) ?>
							</div>
							<div class="col-md-3">
								<?= $form->field($modelLease, 'condition_id')->dropDownList(
									ArrayHelper::map(Condition::find()->all(),'id','name'),
									['prompt' => 'Выберите состояние']
								) ?>
							</div>
							<div class="col-md-3">
								<?= $form->field($modelLease, 'status')->dropDownList(
									ArrayHelper::toArray(Book::getStatusesArray()), ['prompt' => 'Укажите статус']) ?>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="row">
							<?= Html::submitButton($action == 'create' ? 'Создать' : 'Сохранить',
								['class' => 'btn btn-success col-md-offset-2 col-md-9', 'style' => 'margin-top: 25px;']) ?>
						</div>
						<div class="row">
							<?= Html::resetButton('Очистить',
								[
									'class' => 'btn btn-default col-md-offset-2 col-md-9', 'style' => 'margin-top: 40px;',
									'onclick' => "location.href='/leases'"
								]) ?>
						</div>
					</div>
				</div>

			<?php ActiveForm::end(); ?>


			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'layout'=>"{pager}\n{summary}\n{items}\n{pager}",
				'pager' => [
					'maxButtonCount' => 10,
					'nextPageLabel' => false,
					'prevPageLabel' => false,
				],
				'tableOptions' => [
					'class' => 'table table-striped table-bordered'
				],
				'columns' => [
					[
						'attribute' => 'id',
						'headerOptions' => ['width' => '70'],
					],
					[
						'label' => 'Книга',
						'attribute' => 'book_id',
						'value' => function ($row) {
							return $row->book->name;
						},
						'filter' => ArrayHelper::map(Book::find()->all(),'id','name'),
					],
					[
						'label' => 'Сотрудник',
						'attribute' => 'user_id',
						'value' => function ($row) {
							return $row->user->fullname;
						},
						'filter' => ArrayHelper::map(User::find()->all(),'id','fullname'),
						'headerOptions' => ['width' => '180'],
					],
					[
						'label' => 'Состояние',
						'attribute' => 'condition_id',
						'value' => function ($row) {
							return $row->condition->name;
						},
						'filter' => ArrayHelper::map(Condition::find()->all(),'id','name'),
						'headerOptions' => ['width' => '160'],
					],
					[
						'label' => 'Выдана',
						'attribute' => 'issue_at',
						'value' => function ($row) {
							return Yii::$app->formatter->asDate($row->issue_at);
						},
						'filter' => DatePicker::widget([
							'model' => $searchModel,
							'attribute' => 'issue_at',
							'language' => 'ru',
							'dateFormat' => 'yyyy-MM-dd',
							'options' => ['class' => 'form-control'],
						]),
						'format' => 'html',
						'headerOptions' => ['width' => '110'],
					],
					[
						'label' => 'Возврат',
						'attribute' => 'return_at',
						'value' => function ($row) {
							return Yii::$app->formatter->asDate($row->return_at);
						},
						'filter' => DatePicker::widget([
							'model' => $searchModel,
							'attribute' => 'return_at',
							'language' => 'ru',
							'dateFormat' => 'yyyy-MM-dd',
							'options' => ['class' => 'form-control']
						]),
						'format' => 'html',
						'headerOptions' => ['width' => '110'],
					],
					[
						'attribute' => 'status',
						'value' => function ($row) {
							return ArrayHelper::getValue(Book::getStatusesArray(), $row->book->status);
						},
						'filter' => ArrayHelper::toArray(Book::getStatusesArray()),
						'headerOptions' => ['width' => '110'],
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'headerOptions' => ['width' => '50'],
						'template' => '{update} {delete}',
						'urlCreator' => function ($action, $model, $key, $index) {
							$url = '/leases?action=' . $action . '&id=' . $model->id;
							return $url;
						},
					],
				],
			]); ?>

		<?php Pjax::end(); ?>
	</div>

<?php

$js = <<<JS
JS;

$css = <<<CSS
CSS;

$this->registerJs($js);
$this->registerCss($css);
